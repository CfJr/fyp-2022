import tkinter as tk                    
from tkinter import ttk
from tkinter import *
from utilities import *
from midiManager import *
from tkSliderWidget import Slider
from instruments import *
from chords import *
from idlelib.tooltip import Hovertip
  
  
root = tk.Tk()
root.title("Tab Widget")
tabControl = ttk.Notebook(root, width=1000, height=600)

mainTab = tk.Frame(tabControl, bg='#F4F1E9')
utilTab = tk.Frame(tabControl)

listbox = Listbox(mainTab, width=50, height=20, font=('Times', 14))

order = []
manager = midiManager(listbox)
currentSequence = recaman(manager.getSequenceLength())
increasingSequences = getIncreasingSequences(currentSequence)
decreasingSequences = getDecreasingSequences(currentSequence)
defaultOptions = getDefault()
percussionOptions = getPercussion()
  
tabControl.add(mainTab, text ='Main')
tabControl.add(utilTab, text ='Util')
tabControl.pack(expand = 1, fill ="both")

def getSequenceLength():  
	global lengthLabel

	if(lengthEntry.get() == ""):
		return

	length = int(lengthEntry.get())

	if(length < 1):
		return

	manager.setSequence(recaman(length))

	manager.setSequenceLength(length)

	checkAll()

def getInstrument():  
	global instrumentLabel

	if(instrumentEntry.get() == ""):
		return

	instrument = int(instrumentEntry.get())

	if(instrument < 0):
		return

	instrumentLabel.config(text="Current: " + str(instrument))

	manager.setInstrument(instrument)

def getSliders():
	global slider
	
	manager.setMinScaleVal(slider.getValues()[0])
	manager.setMaxScaleVal(slider.getValues()[1])
	
	manager.createMIDI()

def manageFunction(state, func):
	if(state.get() == 1):
		order.append(func)

	if(state.get() == 0):
		order.remove(func)

	checkAll()

def manageParamsFunction(state, func1, func2):
	if(state.get() == 1):
		manager.addMIDIParam(func1)
		manager.addPlayParam(func2)

	if(state.get() == 0):
		manager.removeMIDIParam(func1)
		manager.removePlayParam(func2)


def getSubSequences():
	global increasingSequences, decreasingSequences
	increasingSequences = getIncreasingSequences(manager.getSequence())
	decreasingSequences = getDecreasingSequences(manager.getSequence())


def checkAll():
	global order, increasingDict

	manager.setSequence(recaman(manager.getSequenceLength()))

	for i in range(len(order)):
		manager.setSequence(order[i](manager.getSequence()))

	getSubSequences()

	#lengthLabel.config(text="Current: " + str(manager.getSequenceLength()) + " (" + str(len(manager.getSequence())) + ")")

def setFlam():
	if(flamVar.get() == 1):
		manager.setPercussionHits(2)
	else:
		manager.setPercussionHits(1)

def setFlam2():
	if(flamVar2.get() == 1):
		manager.setPercussionHits2(2)
	else:
		manager.setPercussionHits2(1)

def onKeyPress(event):
	if(event.char == ' '):
		if(manager.getPaused()):
			manager.unpauseMIDI()
		else:
			manager.setPaused(True)

	if(event.char == 'a' or event.char == 'A'):
		if(manager.getPlayPosition() > 0):
			manager.setPlayPosition(manager.getPlayPosition()-1)

	if(event.char == 'd' or event.char == 'D'):
		if(manager.getPlayPosition() < manager.getSequenceLength()-1):
			manager.setPlayPosition(manager.getPlayPosition()+1)

def ex():
	print("Hi");

def test():
	print("Test")

	# #Flatten Sequence to range of scale
	# tempSequence = manager.getSequence().copy()
	# for i in range(len(tempSequence)):
	# 	tempSequence[i] = (tempSequence[i] % (manager.getMaxScaleVal() - manager.getMinScaleVal())) + manager.getMinScaleVal()

	# for i in range(9):
	# 	for chord in chords:
	# 		numberedChord = chords[chord]
	# 		for j in range(len(numberedChord)):
	# 			numberedChord[j] += (i*12)
	# 		if(isSubList(numberedChord, tempSequence)):
	# 			manager.playChord(chord, i)

##################    Main Tab
mainFrame1 = LabelFrame(mainTab, bg="#B1D182", width=470, height=360)
mainFrame1.place(x=20, y=20)

mainFrame1a = LabelFrame(mainTab, bg="#688F4E", width=450, height=100)
mainFrame1a.place(x=30, y=30)

mainFrame1b = LabelFrame(mainTab, bg="#688F4E", width=450, height=100)
mainFrame1b.place(x=30, y=145)

mainFrame1c = LabelFrame(mainTab, bg="#688F4E", width=450, height=100)
mainFrame1c.place(x=30, y=260)

mainFrame2 = LabelFrame(mainTab, bg="#B1D182", width=470, height=120)
mainFrame2.place(x=20, y=390)

mainFrame2a = LabelFrame(mainTab, bg="#688F4E", width=450, height=100)
mainFrame2a.place(x=30, y=400)

mainFrame3 = LabelFrame(mainTab, bg="#B1D182", width=470, height=60)
mainFrame3.place(x=20, y=530)

mainFrame3a = LabelFrame(mainTab, bg="#688F4E", width=265, height=40)
mainFrame3a.place(x=30, y=540)
##################    Util Tab
utilFrame1 = LabelFrame(utilTab, bg="#B1D182", width=330, height=270)
utilFrame1.place(x=20, y=50)

utilFrame1a = LabelFrame(utilTab, bg="#688F4E", width=310, height=35)
utilFrame1a.place(x=30, y=95)

utilFrame1b = LabelFrame(utilTab, bg="#688F4E", width=310, height=35)
utilFrame1b.place(x=30, y=140)

utilFrame1c = LabelFrame(utilTab, bg="#688F4E", width=310, height=35)
utilFrame1c.place(x=30, y=185)

utilFrame1d = LabelFrame(utilTab, bg="#688F4E", width=310, height=35)
utilFrame1d.place(x=30, y=230)

utilFrame1e = LabelFrame(utilTab, bg="#688F4E", width=310, height=35)
utilFrame1e.place(x=30, y=275)

utilFrame2 = LabelFrame(mainTab, bg="#B1D182", width=500, height=360)
utilFrame2.place(x=500, y=20)

utilFrame2a = LabelFrame(mainTab, bg="#688F4E", width=480, height=35)
utilFrame2a.place(x=510, y=65)

utilFrame2b = LabelFrame(mainTab, bg="#688F4E", width=480, height=35)
utilFrame2b.place(x=510, y=110)

utilFrame2c = LabelFrame(mainTab, bg="#688F4E", width=480, height=35)
utilFrame2c.place(x=510, y=155)

utilFrame2d = LabelFrame(mainTab, bg="#688F4E", width=480, height=35)
utilFrame2d.place(x=510, y=200)

utilFrame2e = LabelFrame(mainTab, bg="#688F4E", width=480, height=35)
utilFrame2e.place(x=510, y=245)

utilFrame2f = LabelFrame(mainTab, bg="#688F4E", width=480, height=35)
utilFrame2f.place(x=510, y=290)

utilFrame2g = LabelFrame(mainTab, bg="#688F4E", width=480, height=35)
utilFrame2g.place(x=510, y=335)

utilFrame3 = LabelFrame(utilTab, bg="#B1D182", width=330, height=255)
utilFrame3.place(x=20, y=340)

utilFrame3a = LabelFrame(utilTab, bg="#688F4E", width=310, height=35)
utilFrame3a.place(x=30, y=370)

utilFrame3b = LabelFrame(utilTab, bg="#688F4E", width=310, height=35)
utilFrame3b.place(x=30, y=415)

utilFrame3c = LabelFrame(utilTab, bg="#688F4E", width=310, height=35)
utilFrame3c.place(x=30, y=460)

utilFrame3d = LabelFrame(utilTab, bg="#688F4E", width=310, height=35)
utilFrame3d.place(x=30, y=505)

utilFrame3e = LabelFrame(utilTab, bg="#688F4E", width=310, height=35)
utilFrame3e.place(x=30, y=550)

utilFrame4 = LabelFrame(mainTab, bg="#B1D182", width=500, height=200)
utilFrame4.place(x=500, y=390)

utilFrame4a = LabelFrame(mainTab, bg="#688F4E", width=480, height=35)
utilFrame4a.place(x=510, y=435)

utilFrame4b = LabelFrame(mainTab, bg="#688F4E", width=480, height=35)
utilFrame4b.place(x=510, y=480)

##################

# #Adds entry box for length
# lengthEntry = tk.Entry(utilTab) 
# lengthEntry.place(x=10, y=20)

# #Adds button for inputting length
# lengthButton = tk.Button(utilTab, text='Change Sequence Length', command=getSequenceLength)
# lengthButton.place(x=133, y=16)

# #Adds label for displaying length
# lengthLabel = tk.Label (utilTab, text="Current: " + str(manager.getSequenceLength()) + " (" + str(len(manager.getSequence())) + ")") 
# lengthLabel.place(x=300, y=16)

###########

def setInstrument(value):
	manager.setInstrument(defaultOptions.get(value))

instrumentVariable = StringVar(mainTab)
instrumentVariable.set("Acoustic Grand") # default value
instrumentMenu = OptionMenu(mainTab, instrumentVariable, *defaultOptions, command=setInstrument)
instrumentMenu.place(x=140, y=546)
instrumentMenu.config(highlightbackground="#688F4E", bg="#F4F1E9")

#Adds instrument label
instrumentLabel = tk.Label(mainTab, text="Instrument", bg="#688F4E", fg="#F4F1E9") 
instrumentLabel.place(x=50, y=550)

########### Sequence modifiers

#Adds sequence modifiers label
sequenceModifierLabel = tk.Label(utilTab, text="Sequence Modifiers", bg="#B1D182") 
sequenceModifierLabel.place(x=45, y=60)

#Only Primes Checkbox
onlyPrimesVar = tk.IntVar()
onlyPrimesCB = tk.Checkbutton(utilTab, text='Remove Non Primes', variable=onlyPrimesVar, onvalue=1, offvalue=0, command=lambda: manageFunction(onlyPrimesVar, onlyPrimes), bg="#688F4E", fg="#F4F1E9", selectcolor="#2B463C")
onlyPrimesCB.place(x=40, y=100)

#Remove Primes Checkbox
removePrimesVar = tk.IntVar()
removePrimesCB = tk.Checkbutton(utilTab, text='Remove Primes', variable=removePrimesVar, onvalue=1, offvalue=0, command=lambda: manageFunction(removePrimesVar, removePrimes), bg="#688F4E", fg="#F4F1E9", selectcolor="#2B463C")
removePrimesCB.place(x=40, y=235)

#Remove Evens Checkbox
removeEvensVar = tk.IntVar()
removeEvensCB = tk.Checkbutton(utilTab, text='Remove Evens', variable=removeEvensVar, onvalue=1, offvalue=0, command=lambda: manageFunction(removeEvensVar, removeEvens), bg="#688F4E", fg="#F4F1E9", selectcolor="#2B463C")
removeEvensCB.place(x=40, y=190)

#Remove Odds Checkbox
removeOddsVar = tk.IntVar()
removeOddsCB = tk.Checkbutton(utilTab, text='Remove Odds', variable=removeOddsVar, onvalue=1, offvalue=0, command=lambda: manageFunction(removeOddsVar, removeOdds), bg="#688F4E", fg="#F4F1E9", selectcolor="#2B463C")
removeOddsCB.place(x=40, y=145)

#Remove Duplicates Checkbox
removeDuplicatesVar = tk.IntVar()
removeDuplicatesCB = tk.Checkbutton(utilTab, text='Remove Duplicates', variable=removeDuplicatesVar, onvalue=1, offvalue=0, command=lambda: manageFunction(removeDuplicatesVar, removeDuplicates), bg="#688F4E", fg="#F4F1E9", selectcolor="#2B463C")
removeDuplicatesCB.place(x=40, y=280)

######################
#Adds sub sequence modifiers label
subSequenceModifierLabel = tk.Label(utilTab, text="Sub Sequence Modifiers", bg="#B1D182") 
subSequenceModifierLabel.place(x=45, y=345)

#Remove Decreasing Numbers Checkbox
removeDecreasingVar = tk.IntVar()
removeDecreasingCB = tk.Checkbutton(utilTab, text='Remove Decreasing Sub-Sequences', variable=removeDecreasingVar, onvalue=1, offvalue=0, command=lambda: manageFunction(removeDecreasingVar, removeDecreasingNumbers), bg="#688F4E", fg="#F4F1E9", selectcolor="#2B463C")
removeDecreasingCB.place(x=40, y=375)

#Remove Increasing Numbers Checkbox
removeIncreasingVar = tk.IntVar()
removeIncreasingCB = tk.Checkbutton(utilTab, text='Remove Increasing Sub-Sequences', variable=removeIncreasingVar, onvalue=1, offvalue=0, command=lambda: manageFunction(removeIncreasingVar, removeIncreasingNumbers), bg="#688F4E", fg="#F4F1E9", selectcolor="#2B463C")
removeIncreasingCB.place(x=40, y=420)

#Reverse All Sub-Sequences Checkbox
reverseAllSubSequencesVar = tk.IntVar()
reverseAllSubSequencesCB = tk.Checkbutton(utilTab, text='Reverse All Sub-Sequences', variable=reverseAllSubSequencesVar, onvalue=1, offvalue=0, command=lambda: manageFunction(reverseAllSubSequencesVar, reverseAllSubSequences), bg="#688F4E", fg="#F4F1E9", selectcolor="#2B463C")
reverseAllSubSequencesCB.place(x=40, y=465)

#Reverse Increasing Sub-Sequences Checkbox
reverseIncreasingSubSequencesVar = tk.IntVar()
reverseIncreasingSubSequencesCB = tk.Checkbutton(utilTab, text='Reverse Increasing Sub-Sequences', variable=reverseIncreasingSubSequencesVar, onvalue=1, offvalue=0, command=lambda: manageFunction(reverseIncreasingSubSequencesVar, reverseIncreasingSubSequences), bg="#688F4E", fg="#F4F1E9", selectcolor="#2B463C")
reverseIncreasingSubSequencesCB.place(x=40, y=510)

#Reverse Decreasing Sub-Sequences Checkbox
reverseDecreasingSubSequencesVar = tk.IntVar()
reverseDecreasingSubSequencesCB = tk.Checkbutton(utilTab, text='Reverse Decreasing Sub-Sequences', variable=reverseDecreasingSubSequencesVar, onvalue=1, offvalue=0, command=lambda: manageFunction(reverseDecreasingSubSequencesVar, reverseDecreasingSubSequences), bg="#688F4E", fg="#F4F1E9", selectcolor="#2B463C")
reverseDecreasingSubSequencesCB.place(x=40, y=555)

############################ CHANNEL PARAMS
#Adds sound modifier label
soundModifierLabel = tk.Label(mainTab, text="Sound Modifiers", bg="#B1D182") 
soundModifierLabel.place(x=525, y=30)

#Adds instrument label
instrumentLabel = tk.Label(mainTab, text="Instrument", bg="#B1D182") 
instrumentLabel.place(x=675, y=30)

#Adds interval label
intervalLabel = tk.Label(mainTab, text="Interval", bg="#B1D182") 
intervalLabel.place(x=790, y=400)

#Adds hits label
hitsLabel = tk.Label(mainTab, text="Flam", bg="#B1D182") 
hitsLabel.place(x=855, y=400)

#Adds offset label
offsetLabel = tk.Label(mainTab, text="Offset", bg="#B1D182") 
offsetLabel.place(x=915, y=400)

myBtn = tk.Button(mainTab, text='?', bg="#F4F1E9")
myBtn.place(x=915, y=30)
myTip = Hovertip(myBtn,'Hover over the name of a\n parameter for more information')

myBtn2 = tk.Button(utilTab, text='?', bg="#F4F1E9")
myBtn2.place(x=515, y=30)
myTip = Hovertip(myBtn2,'This tab contains parameters which did not\n suit the sonification process of the sequence.\n They have been kept here as an example of what did not work.')

#Primes Param Checkbox
primesParamVar = tk.IntVar()
primesParamCB = tk.Checkbutton(mainTab, text='Primes', variable=primesParamVar, onvalue=1, offvalue=0, command=lambda: manageParamsFunction(primesParamVar, manager.primesMIDIParam, manager.primesPlayParam), bg="#688F4E", fg="#F4F1E9", selectcolor="#2B463C")
primesParamCB.place(x=520, y=70)

primesTip = Hovertip(primesParamCB,'Plays the selected instrument for every prime number in the sequence')

def setPrimeInstrument(value):
	manager.setPrimesParamInstrument(defaultOptions.get(value))

primeVariable = StringVar(mainTab)
primeVariable.set("Pizzicato Strings") # default value
setPrimeInstrument("Pizzicato Strings")
primeMenu = OptionMenu(mainTab, primeVariable, *defaultOptions, command=setPrimeInstrument)
primeMenu.config(highlightbackground="#688F4E", bg="#F4F1E9")
primeMenu.place(x=655, y=67)


#New High Param Checkbox
newHighParamVar = tk.IntVar()
newHighParamCB = tk.Checkbutton(mainTab, text='New Highest', variable=newHighParamVar, onvalue=1, offvalue=0, command=lambda: manageParamsFunction(newHighParamVar, manager.newHighMIDIParam, manager.newHighPlayParam), bg="#688F4E", fg="#F4F1E9", selectcolor="#2B463C")
newHighParamCB.place(x=520, y=115)

newHighTip = Hovertip(newHighParamCB,'Plays the selected instrument for every value in the sequence that is the new highest seen')

def setNewHighInstrument(value):
	manager.setNewHighParamInstrument(defaultOptions.get(value))

newHighVariable = StringVar(mainTab)
newHighVariable.set("Tubular Bells") # default value
setNewHighInstrument("Tubular Bells")
newHighMenu = OptionMenu(mainTab, newHighVariable, *defaultOptions, command=setNewHighInstrument)
newHighMenu.config(highlightbackground="#688F4E", bg="#F4F1E9")
newHighMenu.place(x=655, y=112)

#Sub High Param Checkbox
subHighParamVar = tk.IntVar()
subHighParamCB = tk.Checkbutton(mainTab, text='Sub-Seq Highest', variable=subHighParamVar, onvalue=1, offvalue=0, command=lambda: manageParamsFunction(subHighParamVar, manager.subHighMIDIParam, manager.subHighPlayParam), bg="#688F4E", fg="#F4F1E9", selectcolor="#2B463C")
subHighParamCB.place(x=520, y=160)

subHighTip = Hovertip(subHighParamCB,'Plays the selected instrument for every value which is the final note in an increasing sub-sequence\n which is found within all general sub-sequences')

def setSubHighInstrument(value):
	manager.setSubHighParamInstrument(defaultOptions.get(value))

subHighVariable = StringVar(mainTab)
subHighVariable.set("Gunshot") # default value
setSubHighInstrument("Gunshot")
subHighMenu = OptionMenu(mainTab, subHighVariable, *defaultOptions, command=setSubHighInstrument)
subHighMenu.config(highlightbackground="#688F4E", bg="#F4F1E9")
subHighMenu.place(x=655, y=157)

#Sub Low Param Checkbox
subLowParamVar = tk.IntVar()
subLowParamCB = tk.Checkbutton(mainTab, text='Sub-Seq Lowest', variable=subLowParamVar, onvalue=1, offvalue=0, command=lambda: manageParamsFunction(subLowParamVar, manager.subLowMIDIParam, manager.subLowPlayParam), bg="#688F4E", fg="#F4F1E9", selectcolor="#2B463C")
subLowParamCB.place(x=520, y=205)

subLowTip = Hovertip(subLowParamCB,'Plays the selected instrument for every value which is the final note in a decreasing sub-sequence\n which is found within all general sub-sequences')

def setSubLowInstrument(value):
	manager.setSubLowParamInstrument(defaultOptions.get(value))

subLowVariable = StringVar(mainTab)
subLowVariable.set("Sitar") # default value
setSubLowInstrument("Sitar")
subLowMenu = OptionMenu(mainTab, subLowVariable, *defaultOptions, command=setSubLowInstrument)
subLowMenu.config(highlightbackground="#688F4E", bg="#F4F1E9")
subLowMenu.place(x=655, y=202)

#Percussion Param Checkbox
percussionParamVar = tk.IntVar()
percussionParamCB = tk.Checkbutton(mainTab, text='Percussion 1', variable=percussionParamVar, onvalue=1, offvalue=0, command=lambda: manageParamsFunction(percussionParamVar, manager.percussionMIDIParam, manager.percussionPlayParam), bg="#688F4E", fg="#F4F1E9", selectcolor="#2B463C")
percussionParamCB.place(x=520, y=440)

percussionTip = Hovertip(percussionParamCB,'Plays the selected percussion instrument every "Interval" notes')

def setPercussionInstrument(value):
	manager.setPercussionParamInstrument(percussionOptions.get(value))

percussionVariable = StringVar(mainTab)
percussionVariable.set("Acoustic Snare") # default value
percussionMenu = OptionMenu(mainTab, percussionVariable, *percussionOptions, command=setPercussionInstrument)
percussionMenu.config(highlightbackground="#688F4E", bg="#F4F1E9")
percussionMenu.place(x=655, y=437)

intervalOptions = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]

def setPercussionInterval(value):
	manager.setPercussionInterval(value)

	if(value <= manager.getOffset()):
		manager.setOffset(0)
		offsetValue.config(text=manager.getOffset())

intervalVariable = StringVar(mainTab)
intervalVariable.set(10) # default value
intervalMenu = OptionMenu(mainTab, intervalVariable, *intervalOptions, command=setPercussionInterval)
intervalMenu.config(highlightbackground="#688F4E", bg="#F4F1E9")
intervalMenu.place(x=790, y=437)

flamVar = tk.IntVar()
flamCB = tk.Checkbutton(mainTab, variable=flamVar, onvalue=1, offvalue=0, command=lambda: setFlam(), bg="#688F4E", fg="#F4F1E9", selectcolor="#2B463C")
flamCB.place(x=860, y=440)

def updateOffsetValue():
	offsetValue.config(text=manager.getOffset())

#Adds offset value label
offsetValue = tk.Label(mainTab, text=manager.getOffset(), bg="#688F4E", fg="#F4F1E9") 
offsetValue.place(x=930, y=440)

#Decrease Offset Button
decreaseOffsetButton = tk.Button(mainTab, text='-', command=lambda: [manager.setOffset(manager.getOffset()-1), updateOffsetValue()], bg="#F1F4E9")
decreaseOffsetButton.place(x=910, y=440)

#Increase Offset Button
increaseOffsetButton = tk.Button(mainTab, text='+', command=lambda: [manager.setOffset(manager.getOffset()+1), updateOffsetValue()], bg="#F1F4E9")
increaseOffsetButton.place(x=950, y=440)

############################################################
#Percussion Param Checkbox
percussionParamVar2 = tk.IntVar()
percussionParamCB2 = tk.Checkbutton(mainTab, text='Percussion 2', variable=percussionParamVar2, onvalue=1, offvalue=0, command=lambda: manageParamsFunction(percussionParamVar2, manager.percussionMIDIParam2, manager.percussionPlayParam2), bg="#688F4E", fg="#F4F1E9", selectcolor="#2B463C")
percussionParamCB2.place(x=520, y=485)

percussion2Tip = Hovertip(percussionParamCB2,'Plays the selected percussion instrument every "Interval" notes')

def setPercussionInstrument2(value):
	manager.setPercussionParamInstrument2(percussionOptions.get(value))

percussionVariable2 = StringVar(mainTab)
percussionVariable2.set("Low Tom") # default value
percussionMenu2 = OptionMenu(mainTab, percussionVariable2, *percussionOptions, command=setPercussionInstrument2)
percussionMenu2.config(highlightbackground="#688F4E", bg="#F4F1E9")
percussionMenu2.place(x=655, y=482)

def setPercussionInterval2(value):
	manager.setPercussionInterval2(value)

	if(value <= manager.getOffset2()):
		manager.setOffset2(0)
		offsetValue2.config(text=manager.getOffset2())

intervalVariable2 = StringVar(mainTab)
intervalVariable2.set(10) # default value
intervalMenu2 = OptionMenu(mainTab, intervalVariable2, *intervalOptions, command=setPercussionInterval2)
intervalMenu2.config(highlightbackground="#688F4E", bg="#F4F1E9")
intervalMenu2.place(x=790, y=482)

flamVar2 = tk.IntVar()
flamCB2 = tk.Checkbutton(mainTab, variable=flamVar2, onvalue=1, offvalue=0, command=lambda: setFlam2(), bg="#688F4E", fg="#F4F1E9", selectcolor="#2B463C")
flamCB2.place(x=860, y=485)

def updateOffsetValue2():
	offsetValue2.config(text=manager.getOffset2())

#Adds offset value label
offsetValue2 = tk.Label(mainTab, text=manager.getOffset2(), bg="#688F4E", fg="#F4F1E9") 
offsetValue2.place(x=930, y=485)

#Decrease Offset Button
decreaseOffsetButton2 = tk.Button(mainTab, text='-', command=lambda: [manager.setOffset2(manager.getOffset2()-1), updateOffsetValue2()], bg="#F1F4E9")
decreaseOffsetButton2.place(x=910, y=485)

#Increase Offset Button
increaseOffsetButton2 = tk.Button(mainTab, text='+', command=lambda: [manager.setOffset2(manager.getOffset2()+1), updateOffsetValue2()], bg="#F1F4E9")
increaseOffsetButton2.place(x=950, y=485)
#################################################################
#Sub-Sequence Param Checkbox
subSequenceParamVar = tk.IntVar()
subSequenceParamCB = tk.Checkbutton(mainTab, text='Sub-Sequences', variable=subSequenceParamVar, onvalue=1, offvalue=0, command=lambda: manageParamsFunction(subSequenceParamVar, manager.subSequenceMIDIParam, manager.subSequencePlayParam), bg="#688F4E", fg="#F4F1E9", selectcolor="#2B463C")
subSequenceParamCB.place(x=520, y=250)

subSequenceTip = Hovertip(subSequenceParamCB,'Plays the selected instrument for every value that\n appears within a sub-sequence')

def setSubSequenceInstrument(value):
	manager.setSubSequenceParamInstrument(defaultOptions.get(value))

subSequenceVariable = StringVar(mainTab)
subSequenceVariable.set("Bassoon") # default value
setSubSequenceInstrument("Bassoon")
subSequenceMenu = OptionMenu(mainTab, subSequenceVariable, *defaultOptions, command=setSubSequenceInstrument)
subSequenceMenu.config(highlightbackground="#688F4E", bg="#F4F1E9")
subSequenceMenu.place(x=655, y=247)

#Increasing Sub-Sequence Param Checkbox
increasingSubSequenceParamVar = tk.IntVar()
increasingSubSequenceParamCB = tk.Checkbutton(mainTab, text='Increasing Sub-Seqs', variable=increasingSubSequenceParamVar, onvalue=1, offvalue=0, command=lambda: manageParamsFunction(increasingSubSequenceParamVar, manager.increasingSubSequenceMIDIParam, manager.increasingSubSequencePlayParam), bg="#688F4E", fg="#F4F1E9", selectcolor="#2B463C")
increasingSubSequenceParamCB.place(x=520, y=295)

increasingSubSequenceTip = Hovertip(increasingSubSequenceParamCB,'Plays the selected instrument for every value that\n appears within an increasing sub-sequence of a general sub-sequence')

def setIncreasingSubSequenceInstrument(value):
	manager.setIncreasingSubSequenceParamInstrument(defaultOptions.get(value))

increasingSubSequenceVariable = StringVar(mainTab)
increasingSubSequenceVariable.set("Banjo") # default value
setIncreasingSubSequenceInstrument("Banjo")
increasingSubSequenceMenu = OptionMenu(mainTab, increasingSubSequenceVariable, *defaultOptions, command=setIncreasingSubSequenceInstrument)
increasingSubSequenceMenu.config(highlightbackground="#688F4E", bg="#F4F1E9")
increasingSubSequenceMenu.place(x=655, y=292)

#Decreasing Sub-Sequence Param Checkbox
decreasingSubSequenceParamVar = tk.IntVar()
decreasingSubSequenceParamCB = tk.Checkbutton(mainTab, text='Decreasing Sub-Seqs', variable=decreasingSubSequenceParamVar, onvalue=1, offvalue=0, command=lambda: manageParamsFunction(decreasingSubSequenceParamVar, manager.decreasingSubSequenceMIDIParam, manager.decreasingSubSequencePlayParam), bg="#688F4E", fg="#F4F1E9", selectcolor="#2B463C")
decreasingSubSequenceParamCB.place(x=520, y=340)

decreasingSubSequenceTip = Hovertip(decreasingSubSequenceParamCB,'Plays the selected instrument for every value that\n appears within a decreasing sub-sequence of a general sub-sequence')

def setDecreasingSubSequenceInstrument(value):
	manager.setDecreasingSubSequenceParamInstrument(defaultOptions.get(value))

decreasingSubSequenceVariable = StringVar(mainTab)
decreasingSubSequenceVariable.set("Orchestral Harp") # default value
setDecreasingSubSequenceInstrument("Orchestral Harp")
decreasingSubSequenceMenu = OptionMenu(mainTab, decreasingSubSequenceVariable, *defaultOptions, command=setDecreasingSubSequenceInstrument)
decreasingSubSequenceMenu.config(highlightbackground="#688F4E", bg="#F4F1E9")
decreasingSubSequenceMenu.place(x=655, y=337)
################## END OF CHANNEL PARAMS

#TESTING
testVar = tk.IntVar()
testCB = tk.Checkbutton(utilTab, text='Test', variable=testVar, onvalue=1, offvalue=0, command=lambda: test())
testCB.place(x=900, y=500)

################################# SLIDERS

#Length Adjustment
def lengthSliderChange(event):  
	global lengthSliderValue

	manager.setSequence(recaman(lengthSliderValue.get()))
	manager.setSequenceLength(lengthSliderValue.get())

	checkAll()

lengthSliderValue = tk.IntVar()
lengthSlider = Scale(mainTab, variable = lengthSliderValue,
           from_ = 20, to = 4000,
           orient = HORIZONTAL, length = 400, command=lengthSliderChange) 
lengthSlider.set(manager.getSequenceLength())
lengthSlider.place(x=50, y=185)
lengthSlider.config(highlightbackground="#688F4E", bg="#688F4E", fg="#F4F1E9", activebackground="#B1D182", troughcolor="#F4F1E9")

#Adds label for length scale
lengthFrameLabel = tk.Label (mainTab, text="Length Adjustment", bg="#688F4E", fg="#F4F1E9") 
lengthFrameLabel.place(x=50, y=155)

def setScaleValues():
	global scaleSlider
	
	manager.setMinScaleVal(scaleSlider.getValues()[0])
	manager.setMaxScaleVal(scaleSlider.getValues()[1])

#Slider for Scale Values
scaleSlider = Slider(mainTab, width = 300, height = 50, min_val = 0, max_val = 127, init_lis = [manager.getMinScaleVal(), manager.getMaxScaleVal()], show_value = True, command = setScaleValues)
scaleSlider.place(x=100, y=300)
#scaleSlider.config(highlightbackground="#688F4E", bg="#688F4E", fg="#F4F1E9", activebackground="#B1D182", troughcolor="#F4F1E9")

#Adds label for slider scale
scaleFrameLabel = tk.Label (mainTab, text="Scale Adjustment", bg="#688F4E", fg="#F4F1E9") 
scaleFrameLabel.place(x=50, y=270)

#Tempo Adjustment
def tempoSliderChange(event):  
	global tempoSliderValue

	manager.setTempo(tempoSliderValue.get())

tempoSliderValue = tk.IntVar()
tempoSlider = Scale(mainTab, variable = tempoSliderValue,
           from_ = 20, to = 1000,
           orient = HORIZONTAL, length = 400, command=tempoSliderChange) 
tempoSlider.set(manager.getTempo())
tempoSlider.place(x=50, y=70)
tempoSlider.config(highlightbackground="#688F4E", bg="#688F4E", fg="#F4F1E9", activebackground="#B1D182", troughcolor="#F4F1E9")

#Adds label for tempo scale
tempoFrameLabel = tk.Label (mainTab, text="Tempo Adjustment", bg="#688F4E", fg="#F4F1E9") 
tempoFrameLabel.place(x=50, y=40)


#Position Adjustment
def positionSliderChange(event):  
	global positionSliderValue

	manager.setPlayPosition(positionSliderValue.get())
	if(manager.getPaused() == False):
		manager.unpauseMIDI()

positionSliderValue = tk.IntVar()
positionSlider = Scale(mainTab, variable = positionSliderValue,
           from_ = 0, to = manager.getSequenceLength(),
           orient = HORIZONTAL, length = 400, command=positionSliderChange) 
positionSlider.set(manager.getPlayPosition())
positionSlider.place(x=50, y=420)
positionSlider.config(highlightbackground="#688F4E", bg="#688F4E", fg="#F4F1E9", activebackground="#B1D182", troughcolor="#F4F1E9")

manager.setPositionSlider(positionSlider)

#Adds label for play scale
playFrameLabel = tk.Label (mainTab, text="Playback Position", bg="#688F4E", fg="#F4F1E9") 
playFrameLabel.place(x=50, y=405)

###################################
#Unpause MIDI Button
# photo = PhotoImage(file = "play.png")
# photo = photo.subsample(20, 20)
# unpauseButton = tk.Button(mainTab, text='Unpause', command=lambda: manager.unpauseMIDI(), image=photo)
unpauseButton = tk.Button(mainTab, text='Play', command=lambda: manager.unpauseMIDI())
unpauseButton.place(x=170, y=470)

#Pause MIDI Button
pauseButton = tk.Button(mainTab, text='Pause', command=lambda: manager.setPaused(True), bg="#F1F4E9")
pauseButton.place(x=220, y=470)

#Create MIDI Button
createButton = tk.Button(mainTab, text='Create File', command=lambda: manager.createMIDI(), bg="#F1F4E9")
createButton.place(x=350, y=550)

#Record MIDI Button
recordButton = tk.Button(mainTab, text='Start Recording', command=lambda: manager.startRecord(), bg="#F1F4E9")
recordButton.place(x=570, y=550)

#Save MIDI Button
saveRecordButton = tk.Button(mainTab, text='Save Recording', command=lambda: manager.saveRecord(), bg="#F1F4E9")
saveRecordButton.place(x=685, y=550)

#Adds label for Recording 
recordLabel = tk.Label(mainTab, text= "STANDBY", bg="#B1D182")
recordLabel.place(x=653, y=525)
manager.setRecordLabel(recordLabel)

# def CurSelet(evt):
# 	if(listbox.size() == 0):
# 		return
	
# 	# value=str((listbox.get(listbox.curselection())))
# 	# print(value)
# 	manager.setPlayPosition(manager.getPlayPosition()-listbox.curselection()[0])
# 	listbox.delete(0, listbox.curselection()[0]-1)
  
# listbox.bind('<<ListboxSelect>>',CurSelet)
  
# # Creating a Scrollbar
# scrollbar = Scrollbar(utilTab)
  
# # Adding Scrollbar to the right
# scrollbar.pack(side = RIGHT, fill = BOTH)

# # Adding Listbox to the right
# listbox.pack(side = RIGHT, fill = BOTH)
      
# # Attaching Listbox to Scrollbar
# # Since we need to have a vertical 
# # scroll we use yscrollcommand
# listbox.config(yscrollcommand = scrollbar.set)
  
# # setting scrollbar command parameter 
# # to listbox.yview method its yview because
# # we need to have a vertical view
# scrollbar.config(command = listbox.yview)

#Run
root.title("Recaman's Sonification")
root.resizable(False, False)
root.bind('<KeyPress>', onKeyPress)
root.mainloop()