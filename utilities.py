# from midiutil.MidiFile import MIDIFile
# import pygame
# import threading

def isPrime(num):
    # if(num < 0):
    #     num *= -1
    
    if(num == 0 or num == 1):
        return False
    
    for i in range(2, num):
        if(num%i == 0):
            return False
    
    return True

def isDuplicate(number, sequence):
	if(number in sequence):
		return True

	return False

def removeDuplicates(sequence):
	temp = []

	for i in range(len(sequence)):
		if(not(isDuplicate(sequence[i], temp))):
			temp.append(sequence[i])

	return temp


def isDecreasingNumber(index, sequence):
    decreasingNumbers = getDecreasingNumbers(sequence)
    
    if(index in decreasingNumbers):
    	return True

    return False

def isIncreasingNumber(index, sequence):
    increasingNumbers = getIncreasingNumbers(sequence)
    
    if(index in increasingNumbers):
    	return True
    	    
    return False

def isInSubSequence(index, sequence):
	if(isIncreasingNumber(index, sequence)):
		return True

	if(isDecreasingNumber(index, sequence)):
		return True

	return False

def removePrimes(currentSequence):
	tempSequence = []
	for i in range(len(currentSequence)):
		if(not(isPrime(currentSequence[i]))):
			tempSequence.append(currentSequence[i])

	return tempSequence

def onlyPrimes(currentSequence):
	tempSequence = []
	for i in range(len(currentSequence)):
		if(isPrime(currentSequence[i])):
			tempSequence.append(currentSequence[i])

	return tempSequence

def removeDecreasingNumbers(currentSequence):
	tempSequence = []
	for i in range(len(currentSequence)):
		if(not(isDecreasingNumber(i, currentSequence))):
			tempSequence.append(currentSequence[i])

	return tempSequence

def onlyDecreasingNumbers(currentSequence):
	tempSequence = []
	for i in range(len(currentSequence)):
		if(isDecreasingNumber(i, currentSequence)):
			tempSequence.append(currentSequence[i])

	return tempSequence

def removeIncreasingNumbers(currentSequence):
	tempSequence = []
	for i in range(len(currentSequence)):
		if(not(isIncreasingNumber(i, currentSequence))):
			tempSequence.append(currentSequence[i])

	return tempSequence

def onlyIncreasingNumbers(currentSequence):
	tempSequence = []
	for i in range(len(currentSequence)):
		if(isIncreasingNumber(i, currentSequence)):
			tempSequence.append(currentSequence[i])

	return tempSequence

def removeEvens(currentSequence):
	tempSequence = []
	for i in range(len(currentSequence)):
		if(currentSequence[i] %2 != 0):
			tempSequence.append(currentSequence[i])

	return tempSequence

def removeOdds(currentSequence):
	tempSequence = []
	for i in range(len(currentSequence)):
		if(currentSequence[i] %2 == 0):
			tempSequence.append(currentSequence[i])

	return tempSequence

def recaman(n):
 
    # Create an array to store terms
    sequence = [0] * n
 
    # First term of the sequence
    # is always 0
    sequence[0] = 0
 
    # Fill remaining terms using
    # recursive formula.
    for i in range(1, n):

        curr = sequence[i-1] - i
        for j in range(0, i):

            # If sequence[i-1] - i is
            # negative or already
            # exists.
            if ((sequence[j] == curr) or curr < 0):
                curr = sequence[i-1] + i
                break

        sequence[i] = curr

    return sequence

def printSequence(currentSequence):
	print('#####################################################################################')
	print(currentSequence)

def getIncreasingNumbers(sequence):
	increasingSequence = []

	for i in range(0, len(sequence)):

		#Increasing
		if(i < 2 and len(sequence) > 3):
			if(sequence[i+2] == sequence[i] + 1):
				increasingSequence.append(i)

		elif(i > len(sequence) - 3):
		  	if(sequence[i-2] == sequence[i] - 1):
		  		increasingSequence.append(i)

		elif((sequence[i-2] == sequence[i] - 1) or (sequence[i+2] == sequence[i] + 1)):
			increasingSequence.append(i)

	return increasingSequence;

def getIncreasingSequences(sequence):
	increasingSequence = getIncreasingNumbers(sequence)
	increasingSequences = []
	currentSequence = []
	trackingSequence = []
	for i in range(0, len(increasingSequence)):
	   if(len(currentSequence) == 0):
	      currentSequence.append(sequence[increasingSequence[i]])
	      trackingSequence.append(increasingSequence[i])
	      continue
	   
	   if(sequence[increasingSequence[i]] == currentSequence[len(currentSequence)-1] + 1):
	      currentSequence.append(sequence[increasingSequence[i]])
	      trackingSequence.append(increasingSequence[i])
	   else:
	      increasingSequences.append(trackingSequence)
	      currentSequence = []
	      trackingSequence = []
	      currentSequence.append(sequence[increasingSequence[i]])
	      trackingSequence.append(increasingSequence[i])

	increasingSequences.append(trackingSequence)
	return increasingSequences;

def getDecreasingSequences(sequence):
	decreasingSequence = getDecreasingNumbers(sequence)
	decreasingSequences = []
	currentSequence = []
	trackingSequence = []
	for i in range(0, len(decreasingSequence)):
	   if(len(currentSequence) == 0):
	      currentSequence.append(sequence[decreasingSequence[i]])
	      trackingSequence.append(decreasingSequence[i])
	      continue
	   
	   if(sequence[decreasingSequence[i]] == currentSequence[len(currentSequence)-1] - 1):
	      currentSequence.append(sequence[decreasingSequence[i]])
	      trackingSequence.append(decreasingSequence[i])
	   else:
	      decreasingSequences.append(trackingSequence)
	      currentSequence = []
	      trackingSequence = []
	      currentSequence.append(sequence[decreasingSequence[i]])
	      trackingSequence.append(decreasingSequence[i])

	decreasingSequences.append(trackingSequence)
	return decreasingSequences;

def getDecreasingNumbers(sequence):
	decreasingSequence = []

	for i in range(0, len(sequence)):

		if(i < 2 and len(sequence) > 3):
			if(sequence[i+2] == sequence[i] - 1):
				decreasingSequence.append(i)

		elif(i > len(sequence) - 3):
			if(sequence[i-2] == sequence[i] + 1):
				decreasingSequence.append(i)

		elif((sequence[i-2] == sequence[i] + 1) or (sequence[i+2] == sequence[i] - 1)):
			decreasingSequence.append(i)

	return decreasingSequence;

def reverseSequence(sequence):
	temp = []
	for i in range(len(sequence)):
		temp.append(sequence[(len(sequence)-1)-i])

	return temp

def reverseSubSequence(sequence, subSequence):
	temp = reverseSequence(subSequence)

	for i in range(len(temp)):
		temp[i] = sequence[temp[i]]

	for i in range(len(subSequence)):
		sequence[subSequence[i]] = temp[i]

	return sequence;

def reverseIncreasingSubSequences(sequence):
	increasingSequences = getIncreasingSequences(sequence)
	for i in range(len(increasingSequences)):
		sequence = reverseSubSequence(sequence, increasingSequences[i])

	return sequence

def reverseDecreasingSubSequences(sequence):
	decreasingSequences = getDecreasingSequences(sequence)
	for i in range(len(decreasingSequences)):
		sequence = reverseSubSequence(sequence, decreasingSequences[i])

	return sequence

# def reverseSingleSubSequences(sequence, subSequences):
# 	for i in range(len(subSequences)):
# 		sequence = reverseSubSequence(sequence, subSequences[i])

# 	return sequence

def reverseAllSubSequences(sequence):
	increasingSequences = getIncreasingSequences(sequence)
	decreasingSequences = getDecreasingSequences(sequence)

	for i in range(len(increasingSequences)):
		sequence = reverseSubSequence(sequence, increasingSequences[i])

	for i in range(len(decreasingSequences)):
		sequence = reverseSubSequence(sequence, decreasingSequences[i])

	return sequence

def isNewHigh(sequence, noteIndex):
	if(noteIndex == 0):
		return True
		
	if((max(sequence[:noteIndex]) != sequence[noteIndex]) and (max(sequence[:noteIndex+1]) == sequence[noteIndex])):
		return True

	return False

def isSubHigh(sequence, noteIndex):



	increasingSequences = getIncreasingSequences(sequence);
	if(len(increasingSequences[0]) == 0):
		return False

	for i in range(len(increasingSequences)):
		if(increasingSequences[i][len(increasingSequences[i])-1] == noteIndex):
			return True

	return False

def isSubLow(sequence, noteIndex):
	decreasingSequences = getDecreasingSequences(sequence);
	if(len(decreasingSequences[0]) == 0):
		return False

	for i in range(len(decreasingSequences)):
		if(decreasingSequences[i][len(decreasingSequences[i])-1] == noteIndex):
			return True

	return False

def isSubList(sublist, list):
	for x in range(len(list) - len(sublist) + 1):
	        if list[x : x + len(sublist)] == sublist:
	            return True
	return False

def testing():
	global currentSequence

	print(currentSequence)


# def createDefaultMIDI(sequence, mf):
#     for i in range(len(sequence)):
#         pitch = (sequence[i] % 88) + 20           # G4 - 5
#         time = i * (1/4)             # start on 1/4 beat
#         duration = 1/4         # 1/4 beat long
#         mf.addNote(track, channel, pitch, time, duration, volume)

#     return mf


# def createMIDI(sequence, defaultInstrument, defaultTempo, defaultScale, minScale):
# 	# create your MIDI object
# 	mf = MIDIFile(1)     # only 1 track
# 	track = 0   # the only track

# 	# tempo = 60

# 	# instrument = 0

# 	time = 0    # start at the beginning
# 	mf.addTrackName(track, time, "Sample Track")
# 	mf.addTempo(track, time, defaultTempo)

# 	# add some notes
# 	channel = 0
# 	volume = 100

# 	mf.addProgramChange(track, channel, time, defaultInstrument)


# 	for i in range(len(sequence)):
# 		pitch = (sequence[i] % defaultScale) + minScale
# 		time = i * (1)             # start on 1 beat
# 		duration = 1         # 1 beat long
# 		mf.addNote(track, channel, pitch, time, duration, volume)

# 	# write it to disk
# 	with open("output.mid", 'wb') as outf:
# 	    mf.writeFile(outf)

# 	my_inline_function()


# def my_inline_function():
#     # do some stuff
#     play_thread = threading.Thread(target=play, name="Player",)
#     play_thread.start()
#     # continue doing stuff

# def play_music(midi_filename):
#   '''Stream music_file in a blocking manner'''
#   clock = pygame.time.Clock()
#   pygame.mixer.music.load(midi_filename)
#   pygame.mixer.music.play()
#   while pygame.mixer.music.get_busy():
#     clock.tick(30) # check if playback has finished

# def play():
# 		#PLAYS MIDI
# 	midi_filename = 'output.mid'

# 	# mixer config
# 	freq = 44100  # audio CD quality
# 	bitsize = -16   # unsigned 16 bit
# 	channels = 2  # 1 is mono, 2 is stereo
# 	buffer = 1024   # number of samples
# 	pygame.mixer.init(freq, bitsize, channels, buffer)

# 	# optional volume 0 to 1.0
# 	pygame.mixer.music.set_volume(0.8)

# 	# listen for interruptions
# 	try:
# 	  # use the midi file you just saved
# 	  play_music(midi_filename)
# 	except KeyboardInterrupt:
# 	  # if user hits Ctrl/C then exit
# 	  # (works only in console mode)
# 	  pygame.mixer.music.fadeout(1000)
# 	  pygame.mixer.music.stop()
# 	  raise SystemExit