from midiutil.MidiFile import MIDIFile
import pygame.midi
import threading
import pretty_midi
import time
from utilities import *
from tkinter import *
from chords import *

class midiManager:
	def __init__(self, listbox):

		pygame.midi.init()
		self.player = pygame.midi.Output(0)

		self.listbox = listbox
		self.instrument = 0
		self.tempo = 240
		self.minScaleVal = 20
		self.maxScaleVal = 108
		self.sequenceLength = 100
		self.sequence = recaman(self.sequenceLength)
		self.MIDIparams = []
		self.playParams = []
		self.primesParamInstrument = 0
		self.newHighParamInstrument = 0
		self.subHighParamInstrument = 0
		self.subLowParamInstrument = 0
		self.percussionParamInstrument = 38
		self.percussionParamInstrument2 = 38
		self.offset = 0;
		self.offset2 = 0;
		self.subSequenceParamInstrument = 0
		self.increasingSubSequenceParamInstrument = 0
		self.decreasingSubSequenceParamInstrument = 0
		#self.interrupt = False
		self.paused = True
		self.playPosition = 0
		self.activeCycle = False
		self.positionSlider = None
		self.record = False
		self.recordMF = None
		self.recordPosition = 0
		self.recordLabel = None
		self.percussionInterval = 10
		self.percussionHits = 1
		self.percussionInterval2 = 10
		self.percussionHits2 = 1

	def getSequence(self):
		return self.sequence

	def setSequence(self, sequence):
		self.sequence = sequence
		self.positionSlider.configure(to=len(self.sequence))

	def getInstrument(self):
		return self.instrument

	def setInstrument(self, instrument):
		self.instrument = instrument

	def getTempo(self):
		return self.tempo

	def setTempo(self, tempo):
		self.tempo = tempo

	def getPlayPosition(self):
		return self.playPosition

	def setPlayPosition(self, position):
		self.positionSlider.set(position)
		self.playPosition = position

	def setPositionSlider(self, slider):
		self.positionSlider = slider

	def getMinScaleVal(self):
		return self.minScaleVal

	def setMinScaleVal(self, minScaleVal):
		self.minScaleVal = minScaleVal

	def getMaxScaleVal(self):
		return self.maxScaleVal

	def setMaxScaleVal(self, maxScaleVal):
		self.maxScaleVal = maxScaleVal

	def getSequenceLength(self):
		return self.sequenceLength

	def setSequenceLength(self, sequenceLength):
		self.sequenceLength = sequenceLength

	def getPrimesParamInstrument(self):
		return self.primesParamInstrument

	def setPrimesParamInstrument(self, instrument):
		self.primesParamInstrument = instrument

	def getNewHighParamInstrument(self):
		return self.newHighParamInstrument

	def setNewHighParamInstrument(self, instrument):
		self.newHighParamInstrument = instrument

	def getSubHighParamInstrument(self):
		return self.subHighParamInstrument

	def setSubHighParamInstrument(self, instrument):
		self.subHighParamInstrument = instrument

	def getSubLowParamInstrument(self):
		return self.subLowParamInstrument

	def setSubLowParamInstrument(self, instrument):
		self.subLowParamInstrument = instrument

	def getPercussionParamInstrument(self):
		return self.percussionParamInstrument

	def setPercussionParamInstrument(self, instrument):
		self.percussionParamInstrument = instrument

	def getPercussionParamInstrument2(self):
		return self.percussionParamInstrument2

	def setPercussionParamInstrument2(self, instrument):
		self.percussionParamInstrument2 = instrument

	def setSubSequenceParamInstrument(self, instrument):
		self.subSequenceParamInstrument = instrument

	def setIncreasingSubSequenceParamInstrument(self, instrument):
		self.increasingSubSequenceParamInstrument = instrument

	def setDecreasingSubSequenceParamInstrument(self, instrument):
		self.decreasingSubSequenceParamInstrument = instrument

	def setOffset(self, value):
		if(value > self.percussionInterval-1):
			return

		if(value < 0):
			return

		self.offset = value

	def getOffset(self):
		return self.offset

	def setOffset2(self, value):
		if(value > self.percussionInterval2-1):
			return

		if(value < 0):
			return

		self.offset2 = value

	def getOffset2(self):
		return self.offset2

	def getPaused(self):
		return self.paused

	def setPaused(self, value):
		self.paused = value

	def getRecord(self):
		return self.record

	def setRecord(self, value):
		self.record = value;

	def getMIDIParams(self):
		return self.MIDIparams

	def addMIDIParam(self, param):
		self.MIDIparams.append(param)

	def removeMIDIParam(self, param):
		self.MIDIparams.remove(param)

	def getPlayParams(self):
		return self.playParams

	def addPlayParam(self, param):
		self.playParams.append(param)

	def removePlayParam(self, param):
		self.playParams.remove(param)

	def setInterrupt(self, value):
		self.interrupt = value

	def setPercussionInterval(self, value):
		self.percussionInterval = value

	def setPercussionInterval2(self, value):
		self.percussionInterval2 = value

	def setPercussionHits(self, value):
		self.percussionHits = value

	def setPercussionHits2(self, value):
		self.percussionHits2 = value

	def setRecordLabel(self, label):
		self.recordLabel = label

	def playChord(self, chord, multiplier):
		for j in range(len(chords.get(chord))):
			self.player.note_on((chords.get(chord)[j]) + (multiplier * 12), 127, 1)

		time.sleep(1)

		for j in range(len(chords.get(chord))):
			self.player.note_off(chords.get(chord)[j] + (multiplier * 12), 127, 1)

	def startRecord(self):
		self.record = True
		self.recordPosition = 0;

		self.recordMF = MIDIFile(1)

		track = 0   # the only track

		time = 0    # start at the beginning

		self.recordMF.addTrackName(track, time, "Sample Track")
		self.recordMF.addTempo(track, time, self.tempo)

		self.recordLabel.config(text="RECORDING")


	def saveRecord(self):
		if(self.record == True):
			self.record = False

		# write it to disk
		with open("recordedMIDI.mid", 'wb') as outf:
			self.recordMF.writeFile(outf)

		self.recordLabel.config(text="STANDBY")

	def manageTempo(self, mf, sequence, noteIndex, track, time, pitch):
		if(noteIndex == 0):
			return mf
		
		#mf.addTempo(track, time, self.tempo+(sequence[noteIndex]-sequence[noteIndex-1]))

		#mf.addTempo(track, time, self.tempo+(noteIndex*5))

		average = (int)(self.maxScaleVal + self.minScaleVal)/2

		if(pitch > average):
			mf.addTempo(track, time, self.tempo + 50)
		else:
			mf.addTempo(track, time, self.tempo - 50)


		return mf

	def primesMIDIParam(self, mf, noteIndex, track, channel, pitch, time, duration, volume):
		if(isPrime(self.sequence[noteIndex])):
			mf.addProgramChange(track, channel+1, time, self.getPrimesParamInstrument())
			mf.addNote(track, channel+1, pitch, time, duration, volume)

		return mf

	def percussionMIDIParam(self, mf, noteIndex, track, channel, pitch, time, duration, volume):
		if((noteIndex % self.percussionInterval) == 0):
			for i in range(self.percussionHits):
				mf.addProgramChange(track, 9, time + (0.5 * i), self.getPercussionParamInstrument())
				mf.addNote(track, 9, self.percussionParamInstrument, time + (0.5 * i), duration/self.percussionHits, volume)

		return mf

	def subSequenceMIDIParam(self, mf, noteIndex, track, channel, pitch, time, duration, volume):
		if(isInSubSequence(noteIndex, self.sequence)):
			mf.addProgramChange(track, channel+4, time, self.subSequenceParamInstrument)
			mf.addNote(track, channel+4, pitch, time, duration, volume)

		return mf

	def increasingSubSequenceMIDIParam(self, mf, noteIndex, track, channel, pitch, time, duration, volume):
		if(isIncreasingNumber(noteIndex, self.sequence)):
			mf.addProgramChange(track, channel+5, time, self.increasingSubSequenceParamInstrument)
			mf.addNote(track, channel+5, pitch, time, duration, volume)

		return mf

	def decreasingSubSequenceMIDIParam(self, mf, noteIndex, track, channel, pitch, time, duration, volume):
		if(isDecreasingNumber(noteIndex, self.sequence)):
			mf.addProgramChange(track, channel+6, time, self.decreasingSubSequenceParamInstrument)
			mf.addNote(track, channel+6, pitch, time, duration, volume)

		return mf

	def percussionMIDIParam2(self, mf, noteIndex, track, channel, pitch, time, duration, volume):
		if((noteIndex % self.percussionInterval2) == 0):
			for i in range(self.percussionHits2):
				mf.addProgramChange(track, 9, time + (0.5 * i), self.getPercussionParamInstrument2())
				mf.addNote(track, 9, self.percussionParamInstrument2, time + (0.5 * i), duration/self.percussionHits2, volume)

		return mf

	def newHighMIDIParam(self, mf, noteIndex, track, channel, pitch, time, duration, volume):
		if(noteIndex == 0):
			return mf

		if(isNewHigh(self.sequence, noteIndex)):
			mf.addProgramChange(track, channel+2, time, self.getNewHighParamInstrument())
			mf.addNote(track, channel+2, pitch, time, duration, volume)

		return mf

	def subHighMIDIParam(self, mf, noteIndex, track, channel, pitch, time, duration, volume):
		if(noteIndex == 0):
			return mf

		if(isSubHigh(self.sequence, noteIndex)):
			mf.addProgramChange(track, channel+3, time, self.getSubHighParamInstrument())
			mf.addNote(track, channel+3, pitch, time, duration, volume)

		return mf

	def subLowMIDIParam(self, mf, noteIndex, track, channel, pitch, time, duration, volume):
		if(noteIndex == 0):
			return mf

		if(isSubLow(self.sequence, noteIndex)):
			mf.addProgramChange(track, channel+4, time, self.getSubLowParamInstrument())
			mf.addNote(track, channel+4, pitch, time, duration, volume)

		return mf

	def primesPlayParam(self, noteIndex, player):
		if(isPrime(self.sequence[noteIndex])):
			pitch = (self.sequence[noteIndex] % (self.maxScaleVal - self.minScaleVal)) + self.minScaleVal
			player.set_instrument(self.primesParamInstrument)
			player.note_on(pitch, 127)
			time.sleep(1/(self.tempo/60))
			player.note_off(pitch, 127)

	def percussionPlayParam(self, noteIndex, player):
		if((noteIndex % self.percussionInterval) == 0):
			time.sleep((1/(self.tempo/60)) * self.offset)
			pitch = (self.sequence[noteIndex] % (self.maxScaleVal - self.minScaleVal)) + self.minScaleVal
			for i in range(self.percussionHits):
				player.note_on(self.percussionParamInstrument, 127, 9) #(pitch, volume, channel) channel 9 for percussion
				time.sleep((1/(self.tempo/60))/self.percussionHits)
				player.note_off(self.percussionParamInstrument, 127, 9)

	def percussionPlayParam2(self, noteIndex, player):
		if((noteIndex % self.percussionInterval2) == 0):
			time.sleep((1/(self.tempo/60)) * self.offset2)
			pitch = (self.sequence[noteIndex] % (self.maxScaleVal - self.minScaleVal)) + self.minScaleVal
			for i in range(self.percussionHits2):
				player.note_on(self.percussionParamInstrument2, 127, 9) #(pitch, volume, channel) channel 9 for percussion
				time.sleep((1/(self.tempo/60))/self.percussionHits2)
				player.note_off(self.percussionParamInstrument2, 127, 9)

	def subSequencePlayParam(self, noteIndex, player):
		if(isInSubSequence(noteIndex, self.sequence)):
			pitch = (self.sequence[noteIndex] % (self.maxScaleVal - self.minScaleVal)) + self.minScaleVal
			player.set_instrument(self.subSequenceParamInstrument)
			player.note_on(pitch, 127)
			time.sleep(1/(self.tempo/60))
			player.note_off(pitch, 127)

	def increasingSubSequencePlayParam(self, noteIndex, player):
		if(isIncreasingNumber(noteIndex, self.sequence)):
			pitch = (self.sequence[noteIndex] % (self.maxScaleVal - self.minScaleVal)) + self.minScaleVal
			player.set_instrument(self.increasingSubSequenceParamInstrument)
			player.note_on(pitch, 127)
			time.sleep(1/(self.tempo/60))
			player.note_off(pitch, 127)

	def decreasingSubSequencePlayParam(self, noteIndex, player):
		if(isDecreasingNumber(noteIndex, self.sequence)):
			pitch = (self.sequence[noteIndex] % (self.maxScaleVal - self.minScaleVal)) + self.minScaleVal
			player.set_instrument(self.decreasingSubSequenceParamInstrument)
			player.note_on(pitch, 127)
			time.sleep(1/(self.tempo/60))
			player.note_off(pitch, 127)

	def newHighPlayParam(self, noteIndex, player):
		if(isNewHigh(self.sequence, noteIndex)):
			pitch = (self.sequence[noteIndex] % (self.maxScaleVal - self.minScaleVal)) + self.minScaleVal
			player.set_instrument(self.newHighParamInstrument)
			player.note_on(pitch, 127)
			time.sleep(1/(self.tempo/60))
			player.note_off(pitch, 127)

	def subHighPlayParam(self, noteIndex, player):
		if(isSubHigh(self.sequence, noteIndex)):
			pitch = (self.sequence[noteIndex] % (self.maxScaleVal - self.minScaleVal)) + self.minScaleVal
			player.set_instrument(self.subHighParamInstrument)
			player.note_on(pitch, 127)
			time.sleep(1/(self.tempo/60))
			player.note_off(pitch, 127)

	def subLowPlayParam(self, noteIndex, player):
		if(isSubLow(self.sequence, noteIndex)):
			pitch = (self.sequence[noteIndex] % (self.maxScaleVal - self.minScaleVal)) + self.minScaleVal
			player.set_instrument(self.subLowParamInstrument)
			player.note_on(pitch, 127)
			time.sleep(1/(self.tempo/60))
			player.note_off(pitch, 127)
			
	def createMIDI(self):
		if(len(self.sequence) == 0):
			return

		# create your MIDI object
		mf = MIDIFile(1)     # only 1 track
		track = 0   # the only track

		time = 0    # start at the beginning
		mf.addTrackName(track, time, "Sample Track")
		mf.addTempo(track, time, self.tempo)

		# add some notes
		channel = 0
		volume = 100

		mf.addProgramChange(track, channel, time, self.instrument)
		#mf.addProgramChange(track, channel, time, defaultInstrument+10)

		for i in range(len(self.sequence)):
			pitch = (self.sequence[i] % (self.maxScaleVal - self.minScaleVal)) + self.minScaleVal
			time = i * (1)             # start on 1 beat
			duration = 1         # 1 beat long
			#mf = self.manageTempo(mf, sequence, i, track, time, pitch);
			mf.addNote(track, channel, pitch, time, duration, volume)

			for j in range(len(self.MIDIparams)):
				mf = self.MIDIparams[j](mf, i, track, channel, pitch, time, duration, volume)
				
		# write it to disk
		with open("output.mid", 'wb') as outf:
		    mf.writeFile(outf)

	def addRecordNote(self, index):
		if(len(self.sequence) == 0):
			return

		# create your MIDI object
		track = 0   # the only track

		time = 0    # start at the beginning

		# add some notes
		channel = 0
		volume = 100

		self.recordMF.addProgramChange(track, channel, time, self.instrument)
		#mf.addProgramChange(track, channel, time, defaultInstrument+10)

		pitch = (self.sequence[index] % (self.maxScaleVal - self.minScaleVal)) + self.minScaleVal
		time = self.recordPosition * (1)             # start on 1 beat
		duration = 1         # 1 beat long
		#mf = self.manageTempo(mf, sequence, i, track, time, pitch);
		self.recordMF.addTempo(track, time, self.tempo)
		self.recordMF.addNote(track, channel, pitch, time, duration, volume)

		for j in range(len(self.MIDIparams)):
			self.recordMF = self.MIDIparams[j](self.recordMF, index, track, channel, pitch, time, duration, volume)

		self.recordPosition += 1;
				
		
	def startMIDI(self):
		if(len(self.sequence) == 0):
			return

		self.listbox.delete(0, END)

		self.setPlayPosition(0)
		# self.playPosition = 0
		self.paused = False	

		if(self.activeCycle == False):
			self.play_inline()

	def unpauseMIDI(self):
		if(len(self.sequence) == 0):
			return

		self.paused = False	

		if(self.activeCycle == False):
			self.play_inline()


	def play_inline(self):
		play_thread = threading.Thread(target=self.play, name="Play")
		play_thread.start()

	def param_inline(self, func, noteIndex, player):
		param_thread = threading.Thread(target=lambda: self.paramPlay(func, noteIndex, player), name="Param Play")
		param_thread.start()

	def play(self):
		self.activeCycle = True
		while ((self.playPosition < len(self.sequence)) and (self.paused == False)):
			tempPosition = self.playPosition
			try:
				if(self.maxScaleVal == self.minScaleVal):
					pitch = 0
				else:
					pitch = (self.sequence[self.playPosition] % (self.maxScaleVal - self.minScaleVal)) + self.minScaleVal

				self.listbox.insert(0, (str(self.playPosition) + "| Sequence Number: " + str(self.sequence[self.playPosition]) + "     |     Note: " + str(pitch)))
				self.player.set_instrument(self.instrument)
				self.player.note_on(pitch, 127)
				for j in range(len(self.playParams)):
					self.param_inline(self.playParams[j], self.playPosition, self.player)
				time.sleep(1/(self.tempo/60))
				self.player.note_off(pitch, 127)

				#check and save to recording
				if(self.record == True):
					self.addRecordNote(self.playPosition)

				#Increment play position
				if(tempPosition == self.playPosition):
					self.setPlayPosition(self.playPosition + 1)
				# self.playPosition += 1
			except:
				print("Interrupted!")
				if(self.sequenceLength < self.playPosition):
					print("Played index exceeds sequence length!")

		self.activeCycle = False
		#del player
		#time.sleep(2)
		#pygame.midi.quit()
		#self.interrupt = False	

	def paramPlay(self, func, noteIndex, player):
		func(noteIndex, player)